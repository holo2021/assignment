﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace Assignment
{
    // This is for assignment #1
    public class DataStorage
    {
        private Dictionary<string, object> dict = new Dictionary<string, object>();
        ReaderWriterLock rwl = new ReaderWriterLock();

        private int timeOut = 100;
        public DataStorage(int timeout)
        {
            timeOut = timeout;
        }

        private int WriteToFile()
        {
            // Write dict contents to file.
            return 0;
        }

        /// <summary>
        /// Takes a string key and value and stores them.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="val"></param>
        public void put(string key, object val)
        {
            try
            {
                rwl.AcquireWriterLock(timeOut);
                try
                {
                    dict[key] = val;
                    WriteToFile();
                }
                finally
                {
                    rwl.ReleaseWriterLock();
                }
            }
            catch (Exception)
            {
                // Catch excetpion in case locking timed out.
            }
        }
        /// <summary>
        /// Takes a string and returns value.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public object get(string key)
        {
            object val = null;
            try
            {
                rwl.AcquireReaderLock(timeOut);
                try
                {
                    if (dict.ContainsKey(key))
                    {
                        val = dict[key];
                   }
                }
                finally
                {
                    rwl.ReleaseReaderLock();
                }
            }
            catch (Exception)
            {
                // Catch excetpion in case locking timed out.
            }
            return val;
        }
    }

    // This is assignment #2
    public class CAT
    {
        public CAT() { }

        /// <summary>
        /// Get current position
        /// </summary>
        /// <returns></returns>
        private double GetCurrentPosition()
        {
            return 0;
        }

        /// <summary>
        /// Move by unit
        /// </summary>
        /// <param name="distanceInUnit"></param>
        private void MovePosition(double distanceInUnit)
        {
        }

        /// <summary>
        /// Calibrate the device with target postiion and tolerance. Assuming postive unit move moves it to the right.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public double Calibrate(double target, double tolerance)
        {
            double lastPos, curPos;
            double moveInUnit = 1.0;
            double rate = 1.0;
            lastPos = curPos = GetCurrentPosition();
            bool first = true;
            while (Math.Abs(target - curPos) > tolerance)
            {
                int dir = target - curPos > 0 ? 1 : -1;
                if (first)
                {
                    moveInUnit = 1;
                    first = false;
                } else
                {
                    moveInUnit = Math.Abs(target - curPos) / rate;
                }
                MovePosition(dir * moveInUnit);
                lastPos = curPos;
                curPos = GetCurrentPosition();
                if (curPos != lastPos)
                {
                    rate = Math.Abs(curPos - lastPos) / moveInUnit;
                }
            }
            return curPos;
        }
    }

    class Program
    {

        static void Main(string[] args)
        {
            // Get/Put
            DataStorage storage = new DataStorage(100);
            storage.put("A", 1);
            var val = storage.get("A");
            Console.WriteLine("{0}", val);

            // CAT
            CAT cat = new CAT();
            cat.Calibrate(0, 0.05);
            Console.ReadLine();
        }
    }
}
